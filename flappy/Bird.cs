﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flappy
{
    class Bird
    {
        public int centerX = 12, centerY = 9;

        public Bird(ref char[][] render)
        {
            render[centerX][centerY] = (char)15;
            //render[centerX - 1][centerY] = (char)30;
            //render[centerX + 1][centerY] = (char)31;
            render[centerX][centerY + 1] = (char)2;
            render[centerX][centerY - 1] = '=';
        }

        public bool fly(ref char[][] render)
        {
            render[centerX][centerY] = '\0';
            //render[centerX - 1][centerY] = '\0';
            //render[centerX + 1][centerY] = '\0';
            render[centerX][centerY + 1] = '\0';
            render[centerX][centerY - 1] = '\0';

            centerX -= 2;

            if (render[centerX][centerY] == '|' || render[centerX][centerY] == '-') return false;//crash
            //if (render[centerX - 1][centerY] == '|' || render[centerX - 1][centerY] == '-') return false;
            //if (render[centerX + 1][centerY] == '|' || render[centerX + 1][centerY] == '-') return false;
            if (render[centerX][centerY + 1] == '|' || render[centerX][centerY + 1] == '-') return false;
            if (render[centerX][centerY - 1] == '|' || render[centerX][centerY - 1] == '-') return false;
            //if (centerX - 1 == -1) return false;

            render[centerX][centerY] = (char)15;
            //render[centerX - 1][centerY] = (char)30;
            //render[centerX + 1][centerY] = (char)31;
            render[centerX][centerY + 1] = (char)2;
            render[centerX][centerY - 1] = '=';

            return true;
        }

        public bool fall(ref char[][] render)
        {
            render[centerX][centerY] = '\0';
            //render[centerX - 1][centerY] = '\0';
            //render[centerX + 1][centerY] = '\0';
            render[centerX][centerY + 1] = '\0';
            render[centerX][centerY - 1] = '\0';

            centerX ++;

            if (render[centerX][centerY] == '|' || render[centerX][centerY] == '-') return false;//crash
            //if (render[centerX - 1][centerY] == '|' || render[centerX - 1][centerY] == '-') return false;
            //if (render[centerX + 1][centerY] == '|' || render[centerX + 1][centerY] == '-') return false;
            if (render[centerX][centerY + 1] == '|' || render[centerX][centerY + 1] == '-') return false;
            if (render[centerX][centerY - 1] == '|' || render[centerX][centerY - 1] == '-') return false;
            if (centerX + 1 == 24) return false;

            render[centerX][centerY] = (char)15;
            //render[centerX - 1][centerY] = (char)30;
            //render[centerX + 1][centerY] = (char)31;
            render[centerX][centerY + 1] = (char)2;
            render[centerX][centerY - 1] = '=';

            return true;
        }

        public void reboot()
        {
            centerX = 12;
            centerY = 15;
        }
    }
}
