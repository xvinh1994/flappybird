﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Flappy
{
    class Pipe
    {
        public int left, right;
        public int upY, downY;
        public int length = 6;

        public Pipe()
        {
            Random rand = new Random();
            upY = rand.Next(18);
            downY = 18 - upY;

            left = 36;
            right = 41;
        }

        public Pipe(Pipe prev)
        {
            Random rand = new Random();
            upY = rand.Next(18);
            downY = 18 - upY;

            left = prev.right + 10;
            right = left + 5;
        }

        public void drawLeft(ref char[][] render)
        {
            for (int i = 0; i < upY; i++)
                render[i][left] = '|';
            for (int i = 0; i < downY; i++)
                render[23 - i][left] = '|';
        }

        public void drawRight(ref char[][] render)
        {
            for (int i = 0; i < downY; i++)
                render[23 - i][right] = '|';

            for (int i = 0; i < upY; i++)
                render[i][right] = '|';
        }

        public void drawCover(ref char[][] render)
        {
            for (int i = 0; i < 6; i++)
            {
                render[upY][right - i] = '-';
                render[upY + 5][right - i] = '-';
            }
        }

        public void clearLeft(ref char[][] render)
        {
            for (int j = 0; j < upY; j++)
            {
                render[j][left + 1] = ' ';
                render[j][right + 1] = ' ';
            }

            for (int i = 0; i < downY; i++)
            {
                render[23 - i][left + 1] = ' ';
                render[23 - i][right + 1] = ' ';
            }

            for (int j = 0; j < 7; j++)
            {
                render[upY][j] = ' ';
                render[upY + 5][j] = ' ';
            }
        }
    }
}
