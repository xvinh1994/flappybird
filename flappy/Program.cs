﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Flappy
{
    class Program
    {
        static char[][] render = new char[24][];
        static List<Pipe> pList = new List<Pipe>();
        static Bird superman;
        static int point = 0;

        static void Main(string[] args)
        {
            Console.BufferWidth = 120;
            Console.WindowWidth = 80;
            Console.CursorVisible = false;

            for (int i = 0; i < 24; i++)
                render[i] = new char[100];

            superman = new Bird(ref render);
            Pipe item = new Pipe();
            pList.Add(item);
            drawPipe();
            show();

            do
            {
                play();
            } while (!isExit());
        }

        static void drawPipe()
        {
            Console.Clear();
            clearRender();
            for (int i = 0; i < pList.Count; i++)
            {
                pList[i].drawLeft(ref render);
                pList[i].drawRight(ref render);
                pList[i].drawCover(ref render);
            }
        }

        static void play()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            ConsoleKeyInfo key;

            setPoint();

            while (true)
            {
                while (!Console.KeyAvailable)
                {
                    if (watch.ElapsedMilliseconds == 300)
                    {
                        pipeMove();

                        if (superman.fall(ref render) == false)
                        {
                            Console.Clear();
                            return;
                        }

                        if ((superman.centerX - 1) - pList[0].right == 0) point++;

                        drawPipe();
                        setPoint();
                        show();
                        watch.Restart();
                    }
                }

                key = Console.ReadKey(true);
                if (superman.fly(ref render) == false)
                {
                    Console.Clear();
                    return;
                }
                pipeMove();

                if ((superman.centerX - 1) - pList[0].right == 0) point++;

                setPoint();
                drawPipe();
                show();
            }
        }

        static void pipeMove()
        {
            for (int i = 0; i < pList.Count; i++)
            {
                pList[i].left--;
                pList[i].right--;

                if (pList[i].left == 0)
                {
                    pList[i].clearLeft(ref render);
                    pList.Remove(pList[i]);
                    i--;
                }
                if (80 - pList[pList.Count - 1].right >= 0)
                {
                    Pipe item = new Pipe(pList[pList.Count - 1]);
                    pList.Add(item);
                }
            }
        }

        static void show()
        {
            for (int i = 0; i < 24; i++)
            {
                Console.WriteLine(render[i]);
            }
        }
        
        static void clearRender()
        {
            for (int i = 0; i < 24; i++)
                for (int j = 0; j < 85; j++)
                    if(render[i][j] == '|' || render[i][j] == '-') render[i][j] = '\0';
        }

        static bool isExit()
        {
            string again = "";
            pList.Clear();

            Console.SetCursorPosition(36, 12);
            Console.WriteLine("GAME OVER");
            Console.SetCursorPosition(25, 13);
            Console.WriteLine("Do you want to play again (yes/no)? ");
            Console.SetCursorPosition(39, 14);
            again = Console.ReadLine();
            Console.Clear();

            if (again.Equals("yes"))
            {
                pList.Clear();
                point = 0;
                superman = new Bird(ref render);
                Pipe item = new Pipe();
                pList.Add(item);
                drawPipe();
                setPoint();
                show();

                return false;
            }
            return true;
        }

        static void setPoint()
        {
            string str = point.ToString();

            for (int i = 0; i < str.Length; i++)
                render[12][40 - str.Length + i] = str[i];
        }
    }
}
